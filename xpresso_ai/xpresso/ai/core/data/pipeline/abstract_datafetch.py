__all__=["AbstractFetcher"]
__author__ = "Srijan Sharma"

from xpresso.ai.core.data.pipeline.abstract_ds_pipeline_component import AbstractDSPipelineComponent
from abc import  abstractmethod

class AbstractFetcher(AbstractDSPipelineComponent):
    "Represent a Data Fetch component"
    def  __init__(self,data, state, results, paused):
        AbstractDSPipelineComponent.__init__(self,data, state, results, paused)

    def start(self):
        """
        Entrypoint for the component
        Returns:

        """
        self.fetch()
        return

    @abstractmethod
    def fetch(self):
        """
        Creats a connection to data source, and pulls the data
        Returns:

        """
        pass